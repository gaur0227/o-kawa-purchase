var gulp = require('gulp');
compass = require('gulp-compass'),
plumber = require('gulp-plumber'),
webserver = require('gulp-webserver'),
connect = require('gulp-connect-php'),
browserSync = require('browser-sync').create();

// webserver
// gulp.task('webserver', function () {
//     gulp.src('.')
//         .pipe(webserver({
//             host: 'localhost',
//             port: 8000,
//             livereload: true
//         }));
// });


// compass
gulp.task('compass', function(){
    gulp.src('assets/sass/**/*.scss')
    .pipe(plumber())
    .pipe(compass({
        config_file: 'config.rb',
        comments: false,
        css: 'assets/css/',
        sass: 'assets/sass/'
    }));
});

// gulp.task('watch', ['sass'], function(){
//   var watcher = gulp.watch('assets/sass/**/*.scss', ['sass']);
//   watcher.on('change', function(event) {
//     console.log('File ' + event.path + ' was ' + event.type + ', running tasks...');
//   });
// });

gulp.task('copy', function(){
  gulp.src('*.html')
    .pipe(gulp.dest('./prod'));
});

//watch
gulp.task('watch', ['compass', 'copy'], function(){
  gulp.watch('assets/sass/*.scss', ['compass']);
  gulp.watch('*.html', ['copy']);
});

gulp.task('serve', function(){
  connect.server({
    root: './prod'
  });
});

gulp.task('connect-sync', function() {
  connect.server({}, function() {
    browserSync.init({
      proxy: '127.0.0.1:8000'
    });
  });
  gulp.watch(['./*.php', './*.css', './*.js']).on('change', browserSync.reload);
});

gulp.task('default', ['watch','serve','connect-sync']);
